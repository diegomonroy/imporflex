<!-- Begin Header -->
	<div class="header">
		<div class="row">
			<div class="small-12 medium-4 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-7 columns columns_no">
				<?php dynamic_sidebar( 'phone' ); ?>
				<div class="moduletable_h3">
					<nav class="top-bar" data-topbar role="navigation">
						<ul class="title-area">
							<li class="name"></li>
							<li class="toggle-topbar menu-icon"><a href="#"><span>Menú</span></a></li>
						</ul>
						<section class="top-bar-section">
							<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
						</section>
					</nav>
				</div>
			</div>
			<div class="small-12 medium-1 columns columns_no">
				<?php dynamic_sidebar( 'years' ); ?>
			</div>
		</div>
	</div>
<!-- End Header -->