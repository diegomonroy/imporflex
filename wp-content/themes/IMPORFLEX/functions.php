<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-normalize-css', get_template_directory_uri() . '/assets/js/foundation/css/normalize.css', false, '5.5.3' );
	wp_enqueue_style( 'foundation-min-css', get_template_directory_uri() . '/assets/js/foundation/css/foundation.min.css', false, '5.5.3' );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/assets/js/jquery/fancybox/jquery.fancybox.css', false, '2.1.5' );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	wp_enqueue_script( 'foundation-modernizr-js', get_template_directory_uri() . '/assets/js/foundation/js/vendor/modernizr.js', false, '5.5.3' );
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/assets/js/foundation/js/vendor/jquery.js', false );
	wp_enqueue_script( 'foundation-min-js', get_template_directory_uri() . '/assets/js/foundation/js/foundation.min.js', false, '5.5.3' );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/assets/js/jquery/fancybox/jquery.fancybox.pack.js', false, '2.1.5' );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/assets/js/jquery/scripts.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
			'products-menu' => __( 'Products Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_h1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Phone',
			'id' => 'phone',
			'before_widget' => '<div class="moduletable_h2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Years',
			'id' => 'years',
			'before_widget' => '<div class="moduletable_h4">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 6;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );