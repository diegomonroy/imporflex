<!-- Begin Content -->
	<div class="content">
		<div class="show-for-small-only">
			<div class="off-canvas-wrap" data-offcanvas>
				<div class="inner-wrap">
					<a class="left-off-canvas-toggle" href="#">Productos</a>
					<aside class="left-off-canvas-menu">
						<div class="left">
							<h3>Productos</h3>
							<?php wp_nav_menu( array( 'theme_location' => 'products-menu' ) ); ?>
						</div>
					</aside>
					<div class="row">
						<div class="small-12 columns woo_products">
							<?php woocommerce_content(); ?>
						</div>
					</div>
					<a class="exit-off-canvas"></a>
				</div>
			</div>
		</div>
		<div class="show-for-medium-up">
			<div class="row">
				<div class="medium-3 columns">
					<div class="left">
						<h3>Productos</h3>
						<?php wp_nav_menu( array( 'theme_location' => 'products-menu' ) ); ?>
					</div>
				</div>
				<div class="medium-9 columns woo_products">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</div>
<!-- End Content -->