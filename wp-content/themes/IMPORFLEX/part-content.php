<!-- Begin Content -->
	<div class="content">
		<div class="show-for-small-only">
			<div class="off-canvas-wrap" data-offcanvas>
				<div class="inner-wrap">
					<a class="left-off-canvas-toggle" href="#">Productos</a>
					<aside class="left-off-canvas-menu">
						<div class="left">
							<h3>Productos</h3>
							<?php wp_nav_menu( array( 'theme_location' => 'products-menu' ) ); ?>
						</div>
					</aside>
					<div class="row">
						<div class="small-12 columns">
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<?php the_content(); ?>
							<?php endwhile; endif; ?>
						</div>
					</div>
					<a class="exit-off-canvas"></a>
				</div>
			</div>
		</div>
		<div class="show-for-medium-up">
			<div class="row">
				<div class="medium-3 columns">
					<div class="left">
						<h3>Productos</h3>
						<?php wp_nav_menu( array( 'theme_location' => 'products-menu' ) ); ?>
					</div>
				</div>
				<div class="medium-9 columns columns_no">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>
<!-- End Content -->